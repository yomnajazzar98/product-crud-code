
package src;
import java.util.ArrayList;
//this class to initialize the data of users and products
public class initialize {
     public static user newUsers(String us , String password){
     ArrayList<user> users = new ArrayList<>();
     users.add(new user("cashir@gmail.com" ,"1234" ,"cashir"));
     users.add(new user("admin@gmail.com" ,"1234" ,"admin"));
     for(user u  : users){
         if (u.getEmail().equals(us)&& u.getPassword().equals(password)){
             return u;
         }
     }
     return null;
}
         public static ArrayList<product> getProducts(){
         ArrayList<product> data = new ArrayList<>();
         data.add(new product("1", "p1", "c1", "toBuy", "1000", "branch1", 4));
         data.add(new product("2", "p2", "c2", "toRetrieve", "2000", "branch2",100));
         data.add(new product("3", "p3", "c3", "toBuy", "3000", "branch3",200));
         data.add(new product("4", "p4", "c4", "toBuy", "4000", "branch4",400));
         data.add(new product("5", "p5", "c5", "toRetrieve", "5000", "branch5",600));
         data.add(new product("6", "p6", "c6", "toRetrieve", "6000", "branch6",1000));
         data.add(new product("7", "p7", "c7", "toBuy", "7000", "branch7", 20));
        return data;
    }
}
