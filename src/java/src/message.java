package src;

import java.io.Serializable;

class message implements Serializable { 
    String status;
    String responseText , token;
    public String getUser_type() {
        return status;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getResponseText() {
        return responseText;
    }
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getMessage() {
        return responseText;
    }
    public void setMessage(String message) {
        this.responseText = message;
    }
}