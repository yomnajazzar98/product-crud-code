package src;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@ApplicationPath("root")
@Path("api")
@Provider
public class server extends Application {

    private static ArrayList<product> PRODUCT;
    private static ArrayList<product> PRODUCT1;
    private static String currentUser;
    //json file path 
    public static String path = "D://db.json";
    public static String urgent = "D://urgent.json";

    //login api
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public String login(@FormParam("email") String email, @FormParam("password") String password) {
        message message = new message();
        user newUser = initialize.newUsers(email, password);
        if (newUser == null) {
            message.status = "-1";
            message.responseText = "Login failed ";
        } else {
            currentUser=newUser.getEmail();
            message.status = newUser.getRole();
            message.token = newUser.getToken();
            message.responseText = "Welcome " + newUser.getEmail();
        }
        return new Gson().toJson(message);
    }

    // get data from json file
    public ArrayList<product> getDataFromFile() {
        try {
            File rootPath = new File(path);
            if (rootPath.exists() == false) {
                rootPath.createNewFile();
                PrintWriter wr = new PrintWriter(rootPath);
                ArrayList<product> data = initialize.getProducts();
                wr.write(new Gson().toJson(data));
                wr.close();
            }
            Scanner scanner = new Scanner(rootPath);
            String line = "";
            while (scanner.hasNextLine()) {
                line += scanner.nextLine() + "\n";
            }
            Gson j = new Gson();
            PRODUCT = j.fromJson(line, new TypeToken<ArrayList<product>>() {
            }.getType());
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        if (PRODUCT == null) {
            PRODUCT = new ArrayList<product>();
        }
        System.out.println(PRODUCT);
        return PRODUCT;
    }
       // get data from urgent file
    public ArrayList<product> getDataFromUrgent() {
        try {
            File rootPath = new File(urgent);
            if (rootPath.exists() == false) {
                rootPath.createNewFile();
                PrintWriter wr = new PrintWriter(rootPath);
                ArrayList<product> data = initialize.getProducts();
                wr.write(new Gson().toJson(data));
                wr.close();
            }
            Scanner scanner = new Scanner(rootPath);
            String line = "";
            while (scanner.hasNextLine()) {
                line += scanner.nextLine() + "\n";
            }
            Gson j = new Gson();
            PRODUCT1 = j.fromJson(line, new TypeToken<ArrayList<product>>() {
            }.getType());
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        if (PRODUCT1 == null) {
            PRODUCT1 = new ArrayList<product>();
        }
        System.out.println(PRODUCT1);
        return PRODUCT1;
    }
    //api to read all products
    @Path("data")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String data() {
        ArrayList<product> items = getDataFromFile();
        moveProduct();
        return new Gson().toJson(items);
    }
 //api to read all urgent data
    @Path("data/urgent")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String urgentData() {
        ArrayList<product> items2 = getDataFromUrgent();
        return new Gson().toJson(items2);
    }
   
    //api to search type,price,branch
    @Path("search/type")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String searchType(@FormParam("category") String category) {
        ArrayList<product> items = getDataFromFile();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getCategory().equals(category)) {
                return "The product: " + items.get(i).getName() + " Is Exist";
            }
        }
        return "doesnt Exist";
    }
    @Path("search/branch")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String searchBranch(@FormParam("branch") String branch) {
        ArrayList<product> items = getDataFromFile();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getBranch().equals(branch)) {
                return "The product: " + items.get(i).getName() + " Is Exist";
            }
        }
        return "doesnt Exist";
    }

    @Path("search/price")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String searchPrice(@FormParam("price") String price) {
        ArrayList<product> items = getDataFromFile();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getPrice().equals(price)) {
                return "The product: " + items.get(i).getName() + " Is Exist";
            }
        }
        return "doesnt Exist";
    }

    //api to edit all info of product
    @Path("data/edit")
    @POST
    public String insert(@FormParam("id") String id, @FormParam("name") String name, @FormParam("category") String category, @FormParam("status") String status,
            @FormParam("price") String price, @FormParam("branch") String branch, @FormParam("item") int item) {
        ArrayList<product> items = getDataFromFile(), temp = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                items.get(i).setName(name);
                items.get(i).setCategory(category);
                items.get(i).setStatus(status);
                items.get(i).setPrice(price);
                items.get(i).setBranch(branch);
                items.get(i).setItem(item);
            }
        }
        String x = new Gson().toJson(items);
        PRODUCT = temp;
        File f = new File(path);
        try {
            PrintWriter printer = new PrintWriter(f);
            printer.write(x);
            printer.close();
        } catch (IOException ex) {
        }
        return "success";
    }
    //api to delete product from json file

    @Path("data/delete")
    @POST
    public String delete(@FormParam("id") String id) {
        ArrayList<product> items = getDataFromFile();
        ArrayList<product> temp = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (!items.get(i).getId().equals(id)) {
                temp.add(items.get(i));
            }
        }
        String x = new Gson().toJson(temp);
        PRODUCT = temp;
        File f = new File(path);
        try {
            PrintWriter printer = new PrintWriter(f);
            printer.write(x);
            printer.close();
        } catch (IOException ex) {
        }
        return "success";
    }
    
      public void moveProduct() {
        ArrayList<product> items = getDataFromFile();
        ArrayList<product> temp = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getItem()<100) {
                         System.out.println("save data1: ");
                        System.out.println(items.get(i));
                temp.add(items.get(i));
            }
        }
         System.out.println("save data2: ");
         System.out.println(temp);
        String x = new Gson().toJson(temp);
        PRODUCT1 = temp;
        File f = new File(urgent);
        try {
            PrintWriter printer = new PrintWriter(f);
            printer.write(x);
            printer.close();
        } catch (IOException ex) {
        }
    }
    // insert new product to json file

    @Path("data/insert")
    @POST
    public String add(@FormParam("id") String id, @FormParam("name") String name, @FormParam("category") String category, @FormParam("status") String status,
            @FormParam("price") String price, @FormParam("branch") String branch, @FormParam("item") int item) {
        product pr = new product(id, name, category, status, price, branch, item);
        ArrayList<product> db = getDataFromFile();
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).getId().equals(id) || db.get(i).getName().equals(name)) {
                return "exist product";
            }
        }
        db.add(pr);
        String data = new Gson().toJson(db);
        File pout = new File(path);
        try {
            PrintWriter printer = new PrintWriter(pout);
            printer.write(data);
            printer.close();
        } catch (IOException ex) {
        }
        return "success";
    }
}
